# Das Spiel 'Türme von Hanoi' von Anfang an

## Ein paar Grundlagen

Die folgenden Elemente der Sprache Groovy werden hier nur so kurz vorgestellt, wie sie in den anschließenden Beispielen verwendet werden. Weitere ausführliche Beschreibung finden sich unter [Groovy Language Documentation](http://docs.groovy-lang.org/next/html/documentation/).

### Kommentare im Programm
```
// Was hinter zwei (/) steht wird nicht ausgeführt und dient nur der Beschreibung
```

### Ausgabe auf dem Bildschirm
```
print "Hallo Welt"    // Ausgabe von 'Hallo Welt'
println "Hallo Welt"  // Ausgabe von 'Hallo Welt' mit Zeilenumbruch
```

### Variablen
Mit `def variablenName` wird eine Variable definiert, und mit `=` wird der Variable ein Wert zugewiesen.
```
def anzahlElemente = 5
def meinName = "Max Mustermann"
def begruessung = "Hallo Welt"
```
Variablen können dann wieder an Stellen verwendet werden, an denen bisher feste Werte standen.
```
print begruessung
```

### Einlesen von der Tastatur
```
def meinName = System.console().readLine('Wie ist dein Name? ')
print "Hallo " + meinName
```

### Verzweigungen
Allgemein
```
if ( Bedingung ) {
  // mache dies
} else {
  // sonst das
}
```
Hier ist der else-Teil optional.

Beispiel
```
def wert
if ( x < 5) {
  wert = "gut"
} else {
  wert = "schlecht"
}
```

Kurzform
```
Bedingung ? /* mache dies */ : /* sonst das */
```
Beispiel
```
def wert = x < 5 ? "gut" : "schlecht"
```

### Schleifen

#### for-Schleife
Allgemein
```
for ( Initialisierung; Abbruchbedingung; Schrittweite) {
  ...
}
```
Beispiel
```
def liste = [1, 2, 3]
for (int index=0; index<3; index++) {
  println liste[index]
}

// oder kurz (in groovy)
liste.each { item ->
  println item
}

// oder mit Zähler
3.times { index ->
  println liste[index]
}
```

#### while-Schleife
Allgemein
```
while( Bedingung ) {
  ...
}
```
Beispiel
```
def eingabe = 0
while (eingabe != 'x') {
  eingabe = System.console().readLine('Gib ein Zeichen ein und bestätige mit ENTER:')
}
```

**Strukturen**

* Menge (Set)
* Liste (List)
* Stapel (Stack)
* Schlange (Queue)
* Zuordnung (Map)

[Mehr zu Strukturen in Groovy](http://docs.groovy-lang.org/next/html/documentation/working-with-collections.html)


## Modellierung und Umsetzung

1. Wir haben einen Stapel von Scheiben mit den Größen 3cm, 2cm, 1cm
```groovy
def stapel = [3, 2, 1]
```

2. Wir haben drei Stapel von denen der erste, wie oben angegeben, drei Steine enthält und die anderen beiden leer sind.
```groovy
def stapelListe = [[3, 2, 1], [], []]
```
